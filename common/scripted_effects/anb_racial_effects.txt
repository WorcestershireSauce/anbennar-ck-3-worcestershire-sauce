﻿#Shows tooltip with racial trait chances for children
child_race_chance_father_tooltip_effect = {
	show_as_tooltip = {
		#Elf-race_human-race_orc group
		if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = race_elf
						}
						$FATHER$ = {
							OR = {
								has_trait = race_half_elf
								has_trait = race_human
								has_trait = race_half_orc
							}
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = race_elf
						}
						$MOTHER$ = {
							OR = {
								has_trait = race_half_elf
								has_trait = race_human
								has_trait = race_half_orc
							}
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = race_half_elf
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = race_orc
						}
						$FATHER$ = {
							OR = {
								has_trait = race_elf
								has_trait = race_half_elf
								has_trait = race_human
								has_trait = race_half_orc
							}
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = race_orc
						}
						$MOTHER$ = {
							OR = {
								has_trait = race_elf
								has_trait = race_half_elf
								has_trait = race_human
								has_trait = race_half_orc
							}
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = race_half_orc
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = race_half_elf
						}
						$FATHER$ = {
							has_trait = race_half_orc
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = race_half_elf
						}
						$MOTHER$ = {
							has_trait = race_half_orc
						}
					}
				}
			}
			random_list = {
				75 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_half_orc
				}
				25 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_half_elf
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = race_half_elf
						}
						$FATHER$ = {
							has_trait = race_human
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = race_half_elf
						}
						$MOTHER$ = {
							has_trait = race_human
						}
					}
				}
			}
			random_list = {
				75 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_human
				}
				25 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_half_elf
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = race_half_orc
						}
						$FATHER$ = {
							has_trait = race_human
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = race_half_orc
						}
						$MOTHER$ = {
							has_trait = race_human
						}
					}
				}
			}
			random_list = {
				25 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_human
				}
				75 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_half_orc
				}
			}
		}
		#Mules
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = race_goblin
						}
						$FATHER$ = {
							OR = {
								has_trait = race_elf
								has_trait = race_half_elf
								has_trait = race_human
								has_trait = race_orc
								has_trait = race_half_orc
							}
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = race_goblin
						}
						$MOTHER$ = {
							OR = {
								has_trait = race_elf
								has_trait = race_half_elf
								has_trait = race_human
								has_trait = race_orc
								has_trait = race_half_orc
							}
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = race_half_goblin
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = race_hobgoblin
						}
						$FATHER$ = {
							OR = {
								has_trait = race_elf
								has_trait = race_half_elf
								has_trait = race_human
								has_trait = race_orc
								has_trait = race_half_orc
							}
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = race_hobgoblin
						}
						$MOTHER$ = {
							OR = {
								has_trait = race_elf
								has_trait = race_half_elf
								has_trait = race_human
								has_trait = race_orc
								has_trait = race_half_orc
							}
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = race_half_hobgoblin
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = race_halfling
						}
						$FATHER$ = {
							has_trait = race_gnome
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = race_halfling
						}
						$MOTHER$ = {
							has_trait = race_gnome
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = race_gnomeling
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = race_troll
						}
						$FATHER$ = {
							has_trait = race_ogre
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = race_troll
						}
						$MOTHER$ = {
							has_trait = race_ogre
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = race_trollkin
		}
		#Light-mules
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = race_orc
						}
						$FATHER$ = {
							has_trait = race_ogre
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = race_orc
						}
						$MOTHER$ = {
							has_trait = race_ogre
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = race_ogrillon
		}
		else_if = {
			limit = {
				$MOTHER$ = {
					has_trait = race_ogrillon
				}
				$FATHER$ = {
					has_trait = race_orc
				}
			}
			random_list = {
				90 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_ogrillon
				}
				10 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_orc
				}
			}
		}
		else_if = {
			limit = {
				$MOTHER$ = {
					has_trait = race_ogrillon
				}
				$FATHER$ = {
					has_trait = race_ogre
				}
			}
			random_list = {
				90 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_ogrillon
				}
				10 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_ogre
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						$MOTHER$ = {
							has_trait = race_goblin
						}
						$FATHER$ = {
							has_trait = race_hobgoblin
						}
					}
					AND = {
						$FATHER$ = {
							has_trait = race_goblin
						}
						$MOTHER$ = {
							has_trait = race_hobgoblin
						}
					}
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = race_lesser_hobgoblin
		}
		else_if = {
			limit = {
				$MOTHER$ = {
					has_trait = race_lesser_hobgoblin
				}
				$FATHER$ = {
					has_trait = race_goblin
				}
			}
			random_list = {
				90 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_lesser_hobgoblin
				}
				10 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_goblin
				}
			}
		}
		else_if = {
			limit = {
				$MOTHER$ = {
					has_trait = race_lesser_hobgoblin
				}
				$FATHER$ = {
					has_trait = race_hobgoblin
				}
			}
			random_list = {
				90 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_lesser_hobgoblin
				}
				10 = {
					custom_tooltip = child_race_tooltip
					add_trait_force_tooltip = race_hobgoblin
				}
			}
		}
		#Harpy
		else_if = {
			limit = {
				$MOTHER$ = {
					has_trait = race_harpy
				}
			}
			custom_tooltip = child_race_tooltip
			add_trait_force_tooltip = race_harpy
		}
	}
}

#Assigns racial traits to characters without one
assign_racial_trait_effect = {
	if = {
		limit = {
			is_elvish_culture = yes
		}
		add_trait = race_elf
	}
	else_if = {
		limit = {
			is_human_culture = yes
		}
		add_trait = race_human
	}
	# else_if = {
		# limit = {
			# is_orcish_culture = yes
		# }
		# add_trait = race_orc
	# }
	else_if = {
		limit = {
			is_dwarvish_culture = yes
		}
		add_trait = race_dwarf
	}
	else_if = {
		limit = {
			is_halfling_culture = yes
		}
		add_trait = race_halfling
	}
	else_if = {
		limit = {
			is_gnomish_culture = yes
		}
		add_trait = race_gnome
	}
	# else_if = {
		# limit = {
			# is_gnoll_culture = yes
		# }
		# add_trait = race_gnoll
	# }
	# else_if = {
		# limit = {
			# is_kobold_culture = yes
		# }
		# add_trait = race_kobold
	# }
	# else_if = {
		# limit = {
			# is_troll_culture = yes
		# }
		# add_trait = race_troll
	# }
	# else_if = {
		# limit = {
			# is_ogre_culture = yes
		# }
		# add_trait = race_ogre
	# }
	# else_if = {
		# limit = {
			# is_goblin_culture = yes
		# }
		# add_trait = race_goblin
	# }
	# else_if = {
		# limit = {
			# is_hobgoblin_culture = yes
		# }
		# add_trait = race_hobgoblin
	# }
	# else_if = {
		# limit = {
			# is_harimari_culture = yes
		# }
		# add_trait = race_harimari
	# }
	# else_if = {
		# limit = {
			# is_centaur_culture = yes
		# }
		# add_trait = race_centaur
	# }
	# else_if = {
		# limit = {
			# is_harpy_culture = yes
			# is_female = yes
		# }
		# add_trait = race_harpy
	# }
	# else_if = {
		# limit = {
			# is_harpy_culture = yes
			# is_male = yes
		# }
		# set_culture = culture:gelkar
		# add_trait = race_human
	# }
}

roll_racial_purity_trait_for_character = {
	if = {
		limit = { 
			has_trait = race_elf
			has_culture = culture:sun_elvish
			NOT = { has_variable = no_purist_trait }
		}
		random_list = {
			90 = {
				try_to_add_racial_purist_effect = yes
			}
			10 = { }
		}
	}
	else_if = {
		limit = {
			has_trait = race_elf
			has_culture = culture:moon_elvish
		}
		random_list = {
			25 = {
				try_to_add_racial_purist_effect = yes
			}
			75 = { }
		}
	}
	else = {
		random_list = {
			5 = {
				try_to_add_racial_purist_effect = yes
			}
			95 = { }
		}
	}
}