﻿olavish = {
	color = { 74 38 44 }
	created = 500.1.1
	parents = { old_gerudian }

	ethos = ethos_bellicose
	heritage = heritage_gerudian
	language = language_gerudian
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_practiced_pirates
		tradition_fp1_northern_stories
		tradition_fp1_performative_honour
		tradition_mountaineers
		tradition_fp1_coastal_warriors
	}
	
	name_list = name_list_olavish
	
	coa_gfx = { norse_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { northern_clothing_gfx fp1_norse_clothing_gfx }
	unit_gfx = { northern_unit_gfx }

	ethnicities = {
		70 = caucasian_northern_blond
		15 = caucasian_northern_ginger
		10 = caucasian_northern_brown_hair
		5 = caucasian_northern_dark_hair
	}
}

reverian = {
	color = { 143 139 173 }
	created = 840.1.1
	parents = { dalric iochander }

	ethos = ethos_bellicose
	heritage = heritage_gerudian
	language = language_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_practiced_pirates
		tradition_hereditary_hierarchy
		tradition_culture_blending
	}
	
	name_list = name_list_reverian
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx northern_clothing_gfx fp1_norse_clothing_gfx }
	unit_gfx = { western_unit_gfx }

	ethnicities = {
		40 = caucasian_northern_blond
		20 = caucasian_northern_ginger
		10 = caucasian_northern_brown_hair
		5 = caucasian_northern_dark_hair

		10 = caucasian_blond
		5 = caucasian_ginger
		5 = caucasian_brown_hair
		5 = caucasian_dark_hair	
	}
}

dalric = {
	color = { 227 170 139 }
	created = 500.1.1
	parents = { old_gerudian }

	ethos = ethos_stoic
	heritage = heritage_gerudian
	language = language_gerudian
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_practiced_pirates
		tradition_fp1_northern_stories
		tradition_fp1_performative_honour
		tradition_seafaring
		tradition_fp1_coastal_warriors
	}
	
	name_list = name_list_dalric
	
	coa_gfx = { norse_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { northern_clothing_gfx fp1_norse_clothing_gfx }
	unit_gfx = { northern_unit_gfx }

	ethnicities = {
		65 = caucasian_northern_blond
		20 = caucasian_northern_ginger
		10 = caucasian_northern_brown_hair
		5 = caucasian_northern_dark_hair
	}
}

taric = {
	color = { 53 80 94 }
	created = 500.1.1
	parents = { old_gerudian }

	ethos = ethos_stoic
	heritage = heritage_gerudian
	language = language_gerudian
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_forest_folk
		tradition_mendicant_mystics
	}
	
	name_list = name_list_dalric
	
	coa_gfx = { norse_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { northern_clothing_gfx fp1_norse_clothing_gfx }
	unit_gfx = { northern_unit_gfx }

	ethnicities = {
		70 = caucasian_northern_blond
		15 = caucasian_northern_ginger
		10 = caucasian_northern_brown_hair
		5 = caucasian_northern_dark_hair
	}
}

old_gerudian = {
	color = { 0 0 0 } #placeholder

	ethos = ethos_stoic
	heritage = heritage_gerudian
	language = language_gerudian
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_practiced_pirates
	}
	
	name_list = name_list_olavish
	
	coa_gfx = { norse_coa_gfx western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { northern_clothing_gfx fp1_norse_clothing_gfx }
	unit_gfx = { northern_unit_gfx }

	ethnicities = {
		70 = caucasian_northern_blond
		15 = caucasian_northern_ginger
		10 = caucasian_northern_brown_hair
		5 = caucasian_northern_dark_hair
	}
}