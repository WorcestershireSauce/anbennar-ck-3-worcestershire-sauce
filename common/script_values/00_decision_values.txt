﻿
# Accuse the Krstjani of Heresy
accuse_the_krstjani_of_heresy_cancellation_cooldown_value = 10
accuse_the_krstjani_of_heresy_failure_cooldown_value = 30
accuse_the_krstjani_of_heresy_critical_failure_cooldown_value = 50

# Negotiate the Danelaw
negotiate_the_danelaw_preferable_opponent_value = { # Anbennar
}

standard_commission_artifact_cooldown_time = {
	value = 1825
	#if = {
	#	limit = {
	#		root.culture = {
	#			has_cultural_parameter = more_frequent_hunts
	#		}
	#	}
	#	multiply = 0.5
	#}
}

found_witch_coven_member_count_value = {
	value = 4
}

found_witch_coven_member_percent_value = {
	value = 0.6
}

found_witch_coven_member_percent_display_value = {
	value = found_witch_coven_member_percent_value
	multiply = 100
}
