﻿namespace = anb_decision_major_events

#I have united the Spanish thrones!
anb_decision_major_events.0001 = { #by Mathilda Bjarnehed
	type = character_event
	title = anb_decision_major_events.0001.t
	desc = anb_decision_major_events.0001.desc
	theme = realm
	left_portrait = {
		character = scope:castellyr_former
		animation = personality_bold
	}

	immediate = {
		play_music_cue = "mx_cue_positive_effect"
		form_castellyr_decision_effects = yes
	}
	
	option = {
		name = anb_decision_major_events.0001.a

		give_nickname = nick_the_motherfather_of_castellyr
	}
}

#Someone united the spanish thrones!
anb_decision_major_events.0002 = { #by Mathilda Bjarnehed
	type = character_event
	title = anb_decision_major_events.0001.t
	desc = {
		desc = anb_decision_major_events.0002.start.desc
		first_valid = {
			triggered_desc = {
				trigger = {
					has_RelationToMe_relation = { CHARACTER = scope:castellyr_former }
				}
				desc = anb_decision_major_events.0002.relation_former.desc
			}
			desc = anb_decision_major_events.0002.former.desc
		}	
		desc = anb_decision_major_events.0002.end.desc		
	}
	theme = realm
	left_portrait = {
		character = scope:castellyr_former
		animation = personality_bold
	}
	
	option = {
		name = name_i_see
	}
}