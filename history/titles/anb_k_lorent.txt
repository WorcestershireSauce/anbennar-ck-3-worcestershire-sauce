
k_lorent = {
	1000.1.1 = { change_development_level = 8 }
	
	900.1.1 = {
		holder = lorentis0001 # Lorevarn
	}
	947.1.9 = {
		holder = 712 # Rewan II
		succession_laws = { equal_law } # was male only
	}
	954.11.16 = {
		holder = lorentis0005 # Reanna I
	}
	975.2.28 = {
		holder = lorentis0006 # Reanna II
	}
	990.9.15 = {
		holder = 28	# Rewan III "the Thorn of the West"
	}
	1014.1.4 = {
		holder = 31	# Kylian
	}
	1015.11.1 = {
		holder = 27	# Ruben, husband of Ioriel
	}
	1020.10.27 = {
		holder = 2	# Rean Siloriel
	}
}

d_ainethan = {
	1000.1.1 = { change_development_level = 10 }
	1002.05.14 = {
		holder = 176 #Roger Aldegarde
		liege = k_lorent
	}
	1019.03.10 = {
		holder = 175 #Adelinde Aldegarde
	}
}

c_ainethan = {
	997.02.01 = {
		holder = 176 #Roger Aldegarde
		liege = k_lorent
	}
	1019.03.10 = {
		holder = 175 #Adelinde Aldegarde
	}
}

c_oldport = {
	1000.1.1 = { change_development_level = 11 }
	
	# some date lorenti10000
	938.4.6 = {
		holder = lorenti10000
		liege = k_lorent
	}
	975.2.28 = {
		holder = lorentis0006 # Reanna II
	}
	990.9.15 = {
		holder = 28	# Rewan III "the Thorn of the West"
	}
	1014.1.4 = {
		holder = 31	# Kylian
	}
	1015.11.1 = {
		holder = 27	# Ruben, husband of Ioriel
	}
	1020.10.27 = {
		holder = 2	#Rean Siloriel
	}
}

c_casthil = {
	1000.1.1 = { change_development_level = 10 }
	1002.12.1 = {
		holder = 163	#Jacques father Lorens
		liege = d_ainethan
	}
}

c_lorentaine = {
	1000.1.1 = { change_development_level = 22 }
}

c_rewanfork = {
	1000.1.1 = { change_development_level = 10 }
	1010.2.16 = {
		liege = k_lorent
		holder = 709
	}
}

c_redfort = {
	1000.1.1 = { change_development_level = 9 }
	1010.2.16 = {
		liege = k_lorent
		holder = 709
	}
}

b_harascilde = {
	1010.2.16 = {
		holder = 709
	}
}

c_lorenith = {
	1000.1.1 = { change_development_level = 9 }
}

c_ionnidar = {
	800.1.1 = {
		liege = k_lorent
	}
	948.2.16 = {
		holder = 707
	}
	1000.1.1 = {
		liege = k_lorent
	}
}

c_rosefield = {
	800.1.1 = {
		liege = k_lorent
	}
	948.2.16 = {
		holder = 708
	}
	1000.1.1 = {
		liege = k_lorent
	}
}

d_redglades = {
	1000.1.1 = { change_development_level = 7 }
	1000.1.1 = {
		liege = k_lorent
		succession_laws = { elven_elective_succession_law elf_only  }
	}
	1018.1.1 = {
		holder = 15	#Ioriel
	}
}

c_ioriellen = {
	1000.1.1 = { change_development_level = 10 }
}

d_rewanwood = {
	990.9.19 = {
		liege = k_lorent
		holder = 104 #Olor Rewantis
	}
	1014.2.6 = {
		holder = 105 #Jaspar Rewantis
	}
}

c_autumnglade = {
	1000.1.1 = {
		liege = "d_redglades"
	}
	1018.1.1 = {
		holder = 535	#Delsaran Gladeguard
	}
}

c_wesmar = {
	1000.1.1 = { change_development_level = 10 }
}

d_lower_bloodwine = {
	1000.1.1 = { change_development_level = 11 }
	1000.1.1 = {
		liege = k_lorent
		holder = 20000	#Thayen
	}
}

c_lower_bloodwine = {
	1000.1.1 = {
		holder = 20000	#Thayen
	}
}

c_pircost = {
	1000.1.1 = {
		holder = 20000	#Thayen
	}
}

c_foxalley = {
	950.1.1 = {
		liege = d_enteben
	}
	988.9.25 = {
		holder = 582	#Roen the Elder Arthelis
	}
	1014.7.14 = {
		holder = 583	#Roen the Younger Arthelis. His father dies in Battle of Morban Flats
	}
}

c_minar = {
	1000.1.1 = { change_development_level = 14 }
	1000.1.1 = {
		holder = 170 # Minaran Archpriestess
	}
}

c_wineport = {
	1000.1.1 = { change_development_level = 16 }
	1020.01.01 = {
		liege = k_lorent
		holder = 180 # Runan
		government = republic_government
	}
}

d_upper_bloodwine = {
	1000.1.1 = { change_development_level = 11 }
	833.7.2 = {
		liege = k_enteben
		holder = 563
	}
	868.8.6 = {
		liege = k_enteben
		holder = 562
	}
	893.8.6 = {
		liege = k_lorent
		holder = 561
	}
	917.8.6 = {
		liege = k_lorent
		holder = 559
	}
	973.8.6 = {
		liege = k_lorent
		holder = 558
	}
	985.8.6 = {
		liege = k_lorent
		holder = 556
	}
	1020.1.2 = {
		liege = k_lorent
		holder = 553
	}
}

c_kyliande = {
	1000.1.1 = { change_development_level = 13 }
	1020.1.2 = {
		liege = k_lorent
		holder = 553
	}
}

c_nurionn = {
	1020.1.2 = {
		liege = k_lorent
		holder = 553
	}
}

c_palevine = {
	1000.1.1 = { change_development_level = 12 }
	969.4.15 = {
		liege = k_lorent
		holder = 705	#Beltram Ventis
	}
	1010.6.21 = {
		liege = k_lorent
		holder = 598	#Andred Ventis
	}
	1020.1.2 = {
		liege = k_lorent
		holder = 554	#Beltram Ventis
	}
}

c_greenfield = {
	1000.1.1 = { change_development_level = 11 }
	969.4.15 = {
		liege = k_lorent
		holder = 705	#Beltram Ventis
	}
	1010.6.21 = {
		liege = k_lorent
		holder = 598	#Andred Ventis
	}
	1020.1.2 = {
		liege = k_lorent
		holder = 554	#Beltram Ventis
	}
}
