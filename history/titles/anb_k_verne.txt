k_verne = {
	1000.1.1 = { change_development_level = 8 }
	# 800.1.1 = {
		# succession_laws = { male_preference_law }
	# }
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = 513	#Arman Vernid
	}
	989.6.30 = {
		holder = 48	#Rocair Vernid
	}
}

c_verne = {
	1000.1.1 = { change_development_level = 10 }
}

d_the_tail = {
	1000.1.1 = { change_development_level = 6 }
}

c_stingport = {
	1000.1.1 = { change_development_level = 8 }
	967.2.21 = {	#whoever Arman's dad is, he dies
		holder = 513	#Arman Vernid
	}
	989.6.30 = {	#Slaughter at Cronesford, Rocair's dad dies
		holder = 48	#Rocair Vernid
	}
	990.10.12 = {	#Sorncost-Pearlsedge join the war and make base at Stingport. Pearlsedge refuses to give it back!
		holder = 3	#Aron Pearlman
	}
}

d_wyvernmark = {
	1000.1.1 = { change_development_level = 7 }
}

c_armanhal = {
	1000.1.1 = { change_development_level = 10 }
}


d_menibor_loop = {
	1000.1.1 = { change_development_level = 10 }
}

c_menibor = {
	1000.1.1 = { change_development_level = 17 }
}


c_napesbay = {
	1000.1.1 = { change_development_level = 9 }
}

d_galeinn = {
	1000.1.1 = { change_development_level = 9 }
}

c_walterton = {
	1000.1.1 = { change_development_level = 9 }
}

c_bellacaire = {
	1000.1.1 = { change_development_level = 16 }
}