d_harpyget = {
	1000.1.1 = { change_development_level = 3 }

	980.7.1 = {
		holder = adad_0001
	}
    1021.4.5 = {
		holder = adad_0002
	}
}

c_ebbusubtu = {
	1000.1.1 = { change_development_level = 3 }

	992.2.6 = {
		holder = zaid_0002
	}
}

c_nasratrub = {
	1000.1.1 = { change_development_level = 2 }

	1000.1.2 = {
		holder = rijascar_0001
	}
}

c_deezim = {
	1000.1.1 = { change_development_level = 2 }

	1000.1.2 = {
		holder = bulati_0001
	}
}