﻿norse = {
	is_shown = {
		has_dlc_feature = the_northern_lords
		# OR = {
			# has_culture = culture:norse
			# AND = {
				# religion = religion:germanic_religion
				# culture = { has_cultural_pillar = heritage_north_germanic }
			# }
		# }
		culture = { has_cultural_pillar = heritage_gerudian } # Anbennar
	}
}

iberian = {
	is_shown = {
		has_dlc_feature = the_fate_of_iberia
		culture = { has_cultural_pillar = heritage_iberian }
	}
}
